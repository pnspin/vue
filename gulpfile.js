const gulp = require("gulp");
const rename = require('gulp-rename')
const clean = require('gulp-clean')
const browserify = require('browserify')
const vueify = require('vueify')
const uglify = require('gulp-uglify')
const sass = require('gulp-sass')
const htmlmin = require('gulp-htmlmin')

const fs = require("fs");

gulp.task("bundle", ()=> {
    if (!fs.existsSync("dist")) {
        fs.mkdirSync("dist")
    }
    
    if (!fs.existsSync("dist/js")) {
        fs.mkdirSync("dist/js")
    }
    
    return browserify("src/js/main.js").
        transform(vueify).
        external("vue").
        bundle().
        pipe(fs.createWriteStream("dist/js/bundle.js"));
    
})

gulp.task("minifyjs", ()=> {
    return gulp.src("dist/js/bundle.js").
        pipe(uglify()).
        pipe(rename("bundle.min.js")).
        pipe(gulp.dest("dist/js"));
})

gulp.task("js", gulp.series("bundle", "minifyjs"));


gulp.task("css", ()=> {
    return gulp.src("src/css/**/*.scss").
        pipe(sass({outputStyle: 'compressed'})).
        pipe(gulp.dest("dist/css"));
})

gulp.task("html", ()=> {
    return gulp.src("src/**/*.html").
        pipe(htmlmin({ collapseWhitespace: true }   )).
        pipe(gulp.dest("dist"));
});

gulp.task("clean", ()=> {
    return gulp.src("dist").
            pipe(clean());
});

gulp.task("default", gulp.parallel("js", "css", "html"));
